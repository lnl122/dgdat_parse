﻿// Copyright © 2016 Antony S. Ovsyannikov aka lnl122
// License: GPL v2.0
// Idea & parsing algoritm: https://github.com/mbry/DgdatToXlsx

using System;

namespace dgdat_parse
{
    class Program
    {
        
        static void Main(string[] args)
        {
            DateTime dt1 = DateTime.Now;
            dgdat d = new dgdat();
            //bool fl = d.OpenCheck("Almetevsk");
            //bool fl = d.OpenCheck("Krasnoyarsk");
            bool fl = d.OpenCheck("Almaty");
            dgdat.Tdata data = d.Read();

            DateTime dt2 = DateTime.Now;
            Console.WriteLine("\r\n===Общее время работы " + (dt2-dt1));

            string name_mask = "777";
            for (int org_idx = 0; org_idx < data.org.Length; org_idx++) {
                if (data.org[org_idx].name.IndexOf(name_mask) != -1) {
                    for(int fil_idx=0; fil_idx < data.fil.Length; fil_idx++) {
                        if(data.fil[fil_idx].org == org_idx + 1) {
                            for (int addr_idx=0; addr_idx < data.fil_address.Length; addr_idx++) {
                                if(data.fil_address[addr_idx].fil == fil_idx + 1) {
                                    Console.WriteLine("OrgName={0}, Street={1}, House={2}, Fil={3}{4}",
                                        data.org[org_idx].name, 
                                        data.street[data.address_elem[data.fil_address[addr_idx].address - 1].street-1].name,
                                        data.address_elem[data.fil_address[addr_idx].address - 1].building,
                                        data.fil[fil_idx].title,
                                        data.fil[fil_idx].office);
                                }
                            }
                        }
                    }
                }
            }

            // выходим
            Console.WriteLine("\r\nдля выхода нажмите любую клавишу...");
            Console.ReadKey();
        }

    }
}
