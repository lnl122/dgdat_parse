﻿// Copyright © 2016 Antony S. Ovsyannikov aka lnl122
// License: GPL v2.0
// Idea & parsing algoritm: https://github.com/mbry/DgdatToXlsx

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace dgdat_parse
{
    class dgdat
    {
        public bool Ready = false;                  // признак готовности работы парсера
        public List<string> cities;                 // названия городов для выбора
        public List<raw> dump = new List<raw>();    // итоговые данные
        public string error = "";

        public struct raw
        {
            public string id;
            public List<string> data;
        }

        private FileStream fp = null;       // поток для чтения файла
        private List<string> filepaths;     // пути к файлам
        private List<dd> datadir = new List<dd>();
        private long root = 0;

        private string[] do_not_export = { "route_", "ctr_", "geo_", "chm_store", "org_banner", "back_splash", "banner_", "_road_", "road_name", "interchange", "logo_picture", "pk_", "map_to_" };

        private struct dd
        {
            public string table;
            public string field;
            public string data;
        }

        public struct Tdata
        {
            public TLists lists;
            public Taddress_elem[] address_elem;
            public Torg[] org;
            public Tfil[] fil;
            public Tfil_contact[] fil_contact;
            public Tmap_drilldown[] map_drilldown;
            public Tbld_info[] bld_info;
            public Tbuilding_address[] building_address;
            public Tservice_org[] service_org;
            public Tbuilding[] building;
            public Tlandmark[] landmark;
            public Tplusones[] plusones;
            public Tfil_rub[] fil_rub;
            public Torg_rub[] org_rub;
            public Trub1[] rub1;
            public Trub2[] rub2;
            public Trub3[] rub3;
            public Trub3_alias[] rub3_alias;
            public Trub3_keyword[] rub3_keyword;
            public Tstreet_alias[] street_alias;
            public Tstreet[] street;
            public Tcity[] city;
            public Tcity_alias[] city_alias;
            public Tcity_type[] city_type;
            public Tsight[] sight;
            public Tsight_alias[] sight_alias;
            public Tfil_address[] fil_address;
            public Tfil_payment[] fil_payment;
            public Tpayment_type[] payment_type;
            public Tterritory[] territory;
            public Tterritory_alias[] territory_alias;
            public Torg_alias[] org_alias;
            public Tcrossroad[] crossroad;
            public Tdistrict[] district;
            public Tlarial[] larial;
            public Tlarial_alias[] larial_alias;
            public Taddress_alias[] address_alias;
            public Textra_attr_alias[] extra_attr_alias;
            public Tmap_layer[] map_layer;
            public Tpoi[] poi;
            public Tstation_type[] station_type;
            public Ttr_type_info[] tr_type_info;
        }
        public Tdata data;
        public struct TLists
        {
            public string[] wrk_time;
            public int[] address_elem_to_map_drilldown;
            public string[] ad_warnings;
            public string[] service_org_type;
            public string[] plusone_type;
            public string[] sight_type;
            public string[] bld_name;
            public string[] bld_purpose;
            public string[] phone_format;
        }

        public struct Taddress_elem
        {
            public string building;
            public int map_oid;
            public int street;
            public int importance;
            public int is_dir;
            public int is_map;
            public int map_x;
            public int map_y;
            public int map_z;
        }
        public struct Torg
        {
            public int id;
            public string adv;
            public int is_service;
            public int main_plusone_fil;
            public string name;
            public int popularity;
        }
        public struct Tfil
        {
            public int org;
            public string office;
            public string title;
            public int wrk_time;
            public string wrk_time_comment;
        }
        public struct Tfil_contact
        {
            public int fil;
            public string comment;
            public string eaddr;
            public string eaddr_name;
            public string eaddr_pure;
            public string grpname;
            public string ph_ext;
            public int ph_format;
            public string phone;
            public int type;
        }
        public struct Tmap_drilldown
        {
            public int address;
            public int map_address;
        }
        public struct Tbld_info
        {
            public int elev_add;
            public int elev_min;
            public int is_landmark;
            public int is_st_entrance;
            public int landmark_priority;
            public int map_oid;
        }
        public struct Tbuilding_address
        {
            public int address;
            public int map_oid;
        }
        public struct Tservice_org
        {
            public int fil;
            public int map_oid;
            public int org;
            public int type;
        }
        public struct Tbuilding
        {
            public int name;
            public string post_index;
            public int purpose;
        }
        public struct Tlandmark
        {
            public int map_oid;
            public string name;
            public string description;
        }
        public struct Tplusones
        {
            public int fil;
            public string alias;
            public string link;
            public string partner;
            public int type;
            public int uid_lower;
            public int uid_upper;
        }
        public struct Tfil_rub
        {
            public int fil;
            public int rub;
        }
        public struct Torg_rub
        {
            public int org;
            public int rub;
        }
        public struct Trub1
        {
            public string name;
            public int priority;
        }
        public struct Trub2
        {
            public string name;
            public int rub1;
        }
        public struct Trub3
        {
            public int id;
            public int popularity;
            public int priority;
            public int rub2;
        }
        public struct Trub3_alias
        {
            public string alias;
            public int rub3;
        }
        public struct Trub3_keyword
        {
            public string kwd;
            public int rub3;
        }
        public struct Tstreet
        {
            public int city;
            public int importance;
            public int is_dir;
            public int is_map;
            public string name;
            public string name_d;
            public string name_v;
        }
        public struct Tstreet_alias
        {
            public string alias;
            public int street;
        }
        public struct Tcity
        {
            public int city_type;
            public int default_city;
            public int map_oid;
            public string name;
        }
        public struct Tcity_alias
        {
            public string alias;
            public int city;
        }
        public struct Tcity_type
        {
            public string name;
            public string name_abbr;
        }
        public struct Tsight
        {
            public string info;
            public int map_oid;
            public int map_oid_bld;
            public string name;
            public int type;
        }
        public struct Tsight_alias
        {
            public string alias;
            public int sight;
        }
        public struct Tfil_address
        {
            public int fil;
            public int address;
        }
        public struct Tfil_payment
        {
            public int fil;
            public int payment;
        }
        public struct Tpayment_type
        {
            public string code;
            public string name;
        }
        public struct Tterritory
        {
            public int map_oid;
            public string name;
        }
        public struct Tterritory_alias
        {
            public string alias;
            public int territory;
        }
        public struct Torg_alias
        {
            public string alias;
            public int org;
        }
        public struct Tcrossroad
        {
            public string name;
            public int map_oid;
            public int importance;
        }
        public struct Tdistrict
        {
            public string name;
            public int map_oid;
            public int city;
        }
        public struct Tlarial
        {
            public string name;
            public int map_oid;
        }
        public struct Tlarial_alias
        {
            public string alias;
            public int larial;
        }
        public struct Taddress_alias
        {
            public string alias;
            public int address;
        }
        public struct Textra_attr_alias
        {
            public string alias;
            public int extra_attr;
        }
        public struct Tmap_layer
        {
            public string code;
            public int add;
            public int mul;
        }
        public struct Tpoi
        {
            public string tag;
            public int building_map_oid;
            public int map_oid;
        }
        public struct Tstation_type
        {
            public string entrance_header_plural;
            public string entrance_header_singl;
            public string name;
            public string tab_title;
            public int type;
        }
        public struct Ttr_type_info
        {
            public string name;
            public int type;
        }

        private void Table_address_elem()
        {
            List<int> address_elem__map_oid = ExportFieldInt("address_elem", "map_oid", 0, 2);
            int cnt = address_elem__map_oid.Count;
            List<string> address_elem__building = ExportFieldStr("address_elem", "building");
            List<int> address_elem__street = ExportFieldInt("address_elem", "street", 0, 1);
            List<int> address_elem__importance = ExportFieldInt("address_elem", "importance", 0, 1);
            List<int> address_elem__is_dir = ExportFieldInt("address_elem", "is_dir", 0, 1);
            List<int> address_elem__is_map = ExportFieldInt("address_elem", "is_map", 0, 1);
            List<int> address_elem__map_x = ExportFieldInt("address_elem", "map_x", 0, 10);
            List<int> address_elem__map_y = ExportFieldInt("address_elem", "map_y", 0, 10);
            List<int> address_elem__map_z = ExportFieldInt("address_elem", "map_z", 0, 10);

            Taddress_elem[] t1 = new Taddress_elem[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].building = address_elem__building[i];
                t1[i].map_oid = address_elem__map_oid[i];
                t1[i].street = address_elem__street[i];
                t1[i].importance = address_elem__importance[i];
                t1[i].is_dir = address_elem__is_dir[i];
                t1[i].is_map = address_elem__is_map[i];
                t1[i].map_x = address_elem__map_x[i];
                t1[i].map_y = address_elem__map_y[i];
                t1[i].map_z = address_elem__map_z[i];
            }
            data.address_elem = t1;

            List<string> q1 = ExportFieldStr("address_alias", "alias", 1, 0);
            List<int> q2 = ExportFieldInt("address_alias", "address", 0, 2);
            int cnt2 = q1.Count;
            Taddress_alias[] t2 = new Taddress_alias[cnt2];
            for (int i = 0; i < cnt2; i++)
            {
                t2[i].alias = q1[i];
                t2[i].address = q2[i];
            }
            data.address_alias = t2;
        }
        private void Table_org()
        {
            List<int> q1 = ExportFieldInt("org", "id", 0, 2);
            List<string> q2 = ExportFieldStr("org", "adv", 0, 3);
            List<int> q3 = ExportFieldInt("org", "is_service", 0, 1);
            List<int> q4 = ExportFieldInt("org", "main_plusone_fil", 0, 2);
            List<string> q5 = ExportFieldStr("org", "name", 1, 0);
            List<int> q6 = ExportFieldInt("org", "popularity", 0, 1);
            int cnt = q1.Count;
            Torg[] t1 = new Torg[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].id = q1[i];
                t1[i].adv = q2[i];
                t1[i].is_service = q3[i];
                t1[i].main_plusone_fil = q4[i];
                t1[i].name = q5[i];
                t1[i].popularity = q6[i];
            }
            data.org = t1;

            List<int> qq1 = ExportFieldInt("org_alias", "org", 0, 2);
            List<string> qq2 = ExportFieldStr("org_alias", "alias", 1, 0);
            int cnt2 = qq1.Count;
            Torg_alias[] tt1 = new Torg_alias[cnt2];
            for (int i = 0; i < cnt2; i++)
            {
                tt1[i].org = qq1[i];
                tt1[i].alias = qq2[i];
            }
            data.org_alias = tt1;
        }
        private void Table_fil()
        {
            List<int> q1 = ExportFieldInt("fil", "org", 0, 1);
            List<string> q3 = ExportFieldStr("fil", "office", 1, 3);
            List<string> q4 = ExportFieldStr("fil", "title", 1, 3);
            List<int> q5 = ExportFieldInt("fil", "wrk_time", 0, 1);
            List<string> q6 = ExportFieldStr("fil", "wrk_time_comment", 1, 3);

            int cnt = q1.Count;
            Tfil[] t1 = new Tfil[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].org = q1[i];
                t1[i].office = q3[i];
                t1[i].title = q4[i];
                t1[i].wrk_time = q5[i];
                t1[i].wrk_time_comment = q6[i];
            }
            data.fil = t1;
        }
        private void Table_fil_contact()
        {
            List<int> q1 = ExportFieldInt("fil_contact", "fil", 0, 1);
            List<string> q2 = ExportFieldStr("fil_contact", "comment", 1, 3);
            List<string> q3 = ExportFieldStr("fil_contact", "eaddr", 1, 0);
            List<string> q4 = ExportFieldStr("fil_contact", "eaddr_name", 1, 3);
            List<string> q5 = ExportFieldStr("fil_contact", "eaddr_pure", 1, 3);
            List<string> q6 = ExportFieldStr("fil_contact", "grpname", 1, 3);
            List<string> q7 = ExportFieldStr("fil_contact", "ph_ext", 1, 3);
            List<int> q8 = ExportFieldInt("fil_contact", "ph_format", 0, 1);
            List<string> q9 = ExportFieldStr("fil_contact", "phone", 1, 0);
            List<int> q10 = ExportFieldInt("fil_contact", "type", 0, 2);

            int cnt = q1.Count;
            Tfil_contact[] t1 = new Tfil_contact[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].fil = q1[i];
                t1[i].comment = q2[i];
                t1[i].eaddr = q3[i];
                t1[i].eaddr_name = q4[i];
                t1[i].eaddr_pure = q5[i];
                t1[i].grpname = q6[i];
                t1[i].ph_ext = q7[i];
                t1[i].ph_format = q8[i];
                t1[i].phone = q9[i];
                t1[i].type = q10[i];
            }
            data.fil_contact = t1;
        }
        private void List_wrk_time()
        {
            List<string> q1 = ExportFieldStr("wrk_time", "schedule", 1, 0);
            int cnt = q1.Count;
            string[] t1 = new string[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i] = q1[i];
            }
            data.lists.wrk_time = t1;
        }
        private void Table_map_drilldown()
        {
            List<int> q1 = ExportFieldInt("map_drilldown", "address", 0, 2);
            List<int> q2 = ExportFieldInt("map_drilldown", "map_address", 0, 2);
            int cnt = q1.Count;
            Tmap_drilldown[] t1 = new Tmap_drilldown[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].address = q1[i];
                t1[i].map_address = q2[i];
            }
            data.map_drilldown = t1;
        }
        private void List_address_elem_to_map_drilldown()
        {
            List<int> q1 = ExportFieldInt("address_elem_to_map_drilldown", "data", 0, 2);
            int cnt = q1.Count;
            int[] t1 = new int[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i] = q1[i];
            }
            data.lists.address_elem_to_map_drilldown = t1;
        }
        private void List_ad_warnings()
        {
            List<string> q1 = ExportFieldStr("ad_warnings", "warning", 1, 0);
            int cnt = q1.Count;
            string[] t1 = new string[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i] = q1[i];
            }
            data.lists.ad_warnings = t1;
        }
        private void Table_bld_info()
        {
            List<int> q1 = ExportFieldInt("bld_info", "elev_add", 0, 1);
            List<int> q2 = ExportFieldInt("bld_info", "elev_min", 0, 1);
            List<int> q3 = ExportFieldInt("bld_info", "is_landmark", 0, 1);
            List<int> q4 = ExportFieldInt("bld_info", "is_st_entrance", 0, 1);
            List<int> q5 = ExportFieldInt("bld_info", "landmark_priority", 0, 2);
            List<int> q6 = ExportFieldInt("bld_info", "map_oid", 0, 2);
            int cnt = q1.Count;
            Tbld_info[] t1 = new Tbld_info[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].elev_add = q1[i];
                t1[i].elev_min = q2[i];
                t1[i].is_landmark = q3[i];
                t1[i].is_st_entrance = q4[i];
                t1[i].landmark_priority = q5[i];
                t1[i].map_oid = q6[i];
            }
            data.bld_info = t1;
        }
        private void Table_building_address()
        {
            List<int> q1 = ExportFieldInt("building_address", "address", 0, 2);
            List<int> q2 = ExportFieldInt("building_address", "map_oid", 0, 1);
            int cnt = q1.Count;
            Tbuilding_address[] t1 = new Tbuilding_address[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].address = q1[i];
                t1[i].map_oid = q2[i];
            }
            data.building_address = t1;
        }
        private void Table_service_org()
        {
            List<int> q1 = ExportFieldInt("service_org", "fil", 0, 2);
            List<int> q2 = ExportFieldInt("service_org", "map_oid", 0, 1);
            List<int> q3 = ExportFieldInt("service_org", "org", 0, 2);
            List<int> q4 = ExportFieldInt("service_org", "type", 0, 1);
            int cnt = q1.Count;
            Tservice_org[] t1 = new Tservice_org[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].fil = q1[i];
                t1[i].map_oid = q2[i];
                t1[i].org = q3[i];
                t1[i].type = q4[i];
            }
            data.service_org = t1;

            List<string> q5 = ExportFieldStr("service_org_type", "name", 1, 0);
            int cnt2 = q5.Count;
            string[] t2 = new string[cnt2];
            for (int i = 0; i < cnt2; i++)
            {
                t2[i] = q5[i];
            }
            data.lists.service_org_type = t2;
        }
        private void Table_building()
        {
            List<int> q1 = ExportFieldInt("building", "name", 0, 2);
            List<string> q2 = ExportFieldStr("building", "post_index", 1, 3);
            List<int> q3 = ExportFieldInt("building", "purpose", 0, 2);
            int cnt = q1.Count;
            Tbuilding[] t1 = new Tbuilding[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].name = q1[i];
                t1[i].post_index = q2[i];
                t1[i].purpose = q3[i];
            }
            data.building = t1;
        }
        private void Table_landmark()
        {
            List<int> q1 = ExportFieldInt("landmark", "map_oid", 0, 2);
            List<string> q2 = ExportFieldStr("landmark", "name", 1, 0);
            List<string> q3 = ExportFieldStr("landmark", "description", 1, 0);
            int cnt = q1.Count;
            Tlandmark[] t1 = new Tlandmark[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].map_oid = q1[i];
                t1[i].name = q2[i];
                t1[i].description = q3[i];
            }
            data.landmark = t1;
        }
        private void Table_plusones()
        {
            List<int> q1 = ExportFieldInt("plusones", "fil", 0, 2);
            List<string> q2 = ExportFieldStr("plusones", "alias", 1, 0);
            List<string> q3 = ExportFieldStr("plusones", "link", 1, 0);
            List<string> q4 = ExportFieldStr("plusones", "partner", 1, 0);
            List<int> q5 = ExportFieldInt("plusones", "type", 0, 2);
            List<int> q6 = ExportFieldInt("plusones", "uid_lower", 0, 2);
            List<int> q7 = ExportFieldInt("plusones", "uid_upper", 0, 1);
            int cnt = q1.Count;
            Tplusones[] t1 = new Tplusones[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].fil = q1[i];
                t1[i].alias = q2[i];
                t1[i].link = q3[i];
                t1[i].partner = q4[i];
                t1[i].type = q5[i];
                t1[i].uid_lower = q6[i];
                t1[i].uid_upper = q7[i];
            }
            data.plusones = t1;

            List<string> q8 = ExportFieldStr("plusone_type", "name", 1, 0);
            int cnt2 = q8.Count;
            string[] t2 = new string[cnt2];
            for (int i = 0; i < cnt2; i++)
            {
                t2[i] = q8[i];
            }
            data.lists.plusone_type = t2;
        }
        private void Table_rubrics()
        {
            List<int> q1 = ExportFieldInt("fil_rub", "fil", 0, 1);
            List<int> q2 = ExportFieldInt("fil_rub", "rub", 0, 2);
            int cnt = q1.Count;
            Tfil_rub[] t1 = new Tfil_rub[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].fil = q1[i];
                t1[i].rub = q2[i];
            }
            data.fil_rub = t1;

            List<int> q3 = ExportFieldInt("org_rub", "org", 0, 1);
            List<int> q4 = ExportFieldInt("org_rub", "rub", 0, 2);
            int cnt2 = q3.Count;
            Torg_rub[] t2 = new Torg_rub[cnt2];
            for (int i = 0; i < cnt2; i++)
            {
                t2[i].org = q3[i];
                t2[i].rub = q4[i];
            }
            data.org_rub = t2;

            List<string> q5 = ExportFieldStr("rub1", "name", 1, 0);
            List<int> q6 = ExportFieldInt("rub1", "priority", 0, 4);
            int cnt3 = q5.Count;
            Trub1[] t3 = new Trub1[cnt3];
            for (int i = 0; i < cnt3; i++)
            {
                t3[i].name = q5[i];
                t3[i].priority = q6[i];
            }
            data.rub1 = t3;

            List<string> q7 = ExportFieldStr("rub2", "name", 1, 0);
            List<int> q8 = ExportFieldInt("rub2", "rub1", 0, 1);
            int cnt4 = q7.Count;
            Trub2[] t4 = new Trub2[cnt4];
            for (int i = 0; i < cnt4; i++)
            {
                t4[i].name = q7[i];
                t4[i].rub1 = q8[i];
            }
            data.rub2 = t4;

            List<string> q9 = ExportFieldStr("rub3_alias", "alias", 1, 0);
            List<int> q10 = ExportFieldInt("rub3_alias", "rub3", 0, 1);
            int cnt5 = q9.Count;
            Trub3_alias[] t5 = new Trub3_alias[cnt5];
            for (int i = 0; i < cnt5; i++)
            {
                t5[i].alias = q9[i];
                t5[i].rub3 = q10[i];
            }
            data.rub3_alias = t5;

            List<string> q11 = ExportFieldStr("rub3_keyword", "kwd", 1, 0);
            List<int> q12 = ExportFieldInt("rub3_keyword", "rub3", 0, 2);
            int cnt6 = q11.Count;
            Trub3_keyword[] t6 = new Trub3_keyword[cnt6];
            for (int i = 0; i < cnt6; i++)
            {
                t6[i].kwd = q11[i];
                t6[i].rub3 = q12[i];
            }
            data.rub3_keyword = t6;

            List<int> q13 = ExportFieldInt("rub3", "id", 0, 2);
            List<int> q14 = ExportFieldInt("rub3", "popularity", 0, 2);
            List<int> q15 = ExportFieldInt("rub3", "priority", 0, 1);
            List<int> q16 = ExportFieldInt("rub3", "rub2", 0, 1);
            int cnt7 = q13.Count;
            Trub3[] t7 = new Trub3[cnt7];
            for (int i = 0; i < cnt7; i++)
            {
                t7[i].id = q13[i];
                t7[i].popularity = q14[i];
                t7[i].priority = q15[i];
                t7[i].rub2 = q16[i];
            }
            data.rub3 = t7;
        }
        private void Table_streets()
        {
            List<int> q1 = ExportFieldInt("street", "city", 0, 1);
            List<int> q2 = ExportFieldInt("street", "importance", 0, 1);
            List<int> q3 = ExportFieldInt("street", "is_dir", 0, 1);
            List<int> q4 = ExportFieldInt("street", "is_map", 0, 1);
            List<string> q5 = ExportFieldStr("street", "name", 1, 0);
            List<string> q6 = ExportFieldStr("street", "name_d", 1, 3);
            List<string> q7 = ExportFieldStr("street", "name_v", 1, 3);
            int cnt = q1.Count;
            Tstreet[] t1 = new Tstreet[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].city = q1[i];
                t1[i].importance = q2[i];
                t1[i].is_dir = q3[i];
                t1[i].is_map = q4[i];
                t1[i].name = q5[i];
                t1[i].name_d = q6[i];
                t1[i].name_v = q7[i];
            }
            data.street = t1;

            List<string> q11 = ExportFieldStr("street_alias", "alias", 1, 0);
            List<int> q12 = ExportFieldInt("street_alias", "street", 0, 2);
            int cnt2 = q11.Count;
            Tstreet_alias[] t2 = new Tstreet_alias[cnt2];
            for (int i = 0; i < cnt2; i++)
            {
                t2[i].alias = q11[i];
                t2[i].street = q12[i];
            }
            data.street_alias = t2;
        }
        private void Table_cities()
        {
            List<int> q1 = ExportFieldInt("city", "city_type", 0, 2);
            List<int> q2 = ExportFieldInt("city", "default_city", 0, 1);
            List<int> q3 = ExportFieldInt("city", "map_oid", 0, 2);
            List<string> q4 = ExportFieldStr("city", "name", 1, 0);
            int cnt = q1.Count;
            Tcity[] t1 = new Tcity[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].city_type = q1[i];
                t1[i].default_city = q2[i];
                t1[i].map_oid = q3[i];
                t1[i].name = q4[i];
            }
            data.city = t1;

            List<string> q5 = ExportFieldStr("city_alias", "alias", 1, 0);
            List<int> q6 = ExportFieldInt("city_alias", "city", 0, 2);
            int cnt2 = q5.Count;
            Tcity_alias[] t2 = new Tcity_alias[cnt2];
            for (int i = 0; i < cnt2; i++)
            {
                t2[i].alias = q5[i];
                t2[i].city = q6[i];
            }
            data.city_alias = t2;

            List<string> q7 = ExportFieldStr("city_type", "name", 1, 0);
            List<string> q8 = ExportFieldStr("city_type", "name_abbr", 1, 0);
            int cnt3 = q7.Count;
            Tcity_type[] t3 = new Tcity_type[cnt3];
            for (int i = 0; i < cnt3; i++)
            {
                t3[i].name = q7[i];
                t3[i].name_abbr = q8[i];
            }
            data.city_type = t3;
        }
        private void Table_sights()
        {
            List<string> q1 = ExportFieldStr("sight", "info", 1, 0);
            List<int> q2 = ExportFieldInt("sight", "map_oid", 0, 2);
            List<int> q3 = ExportFieldInt("sight", "map_oid_bld", 0, 2);
            List<string> q4 = ExportFieldStr("sight", "name", 1, 0);
            List<int> q5 = ExportFieldInt("sight", "type", 0, 1);
            int cnt = q1.Count;
            Tsight[] t1 = new Tsight[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].info = q1[i];
                t1[i].map_oid = q2[i];
                t1[i].map_oid_bld = q3[i];
                t1[i].name = q4[i];
                t1[i].type = q5[i];
            }
            data.sight = t1;

            List<string> q6 = ExportFieldStr("sight_alias", "alias", 1, 0);
            List<int> q7 = ExportFieldInt("sight_alias", "sight", 0, 2);
            int cnt2 = q6.Count;
            Tsight_alias[] t2 = new Tsight_alias[cnt2];
            for (int i = 0; i < cnt2; i++)
            {
                t2[i].alias = q6[i];
                t2[i].sight = q7[i];
            }
            data.sight_alias = t2;

            List<string> q8 = ExportFieldStr("sight_type", "name", 1, 0);
            int cnt3 = q8.Count;
            string[] t3 = new string[cnt3];
            for (int i = 0; i < cnt3; i++)
            {
                t3[i] = q8[i];
            }
            data.lists.sight_type = t3;
        }
        private void Table_fil_address()
        {
            List<int> q1 = ExportFieldInt("fil_address", "fil", 0, 1);
            List<int> q2 = ExportFieldInt("fil_address", "address", 0, 2);
            int cnt = q1.Count;
            Tfil_address[] t1 = new Tfil_address[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].fil = q1[i];
                t1[i].address = q2[i];
            }
            data.fil_address = t1;
        }
        private void Table_fil_payment()
        {
            List<int> q1 = ExportFieldInt("fil_payment", "fil", 0, 1);
            List<int> q2 = ExportFieldInt("fil_payment", "payment", 0, 2);
            int cnt = q1.Count;
            Tfil_payment[] t1 = new Tfil_payment[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].fil = q1[i];
                t1[i].payment = q2[i];
            }
            data.fil_payment = t1;
        }
        private void Table_payment_type()
        {
            List<string> q1 = ExportFieldStr("payment_type", "code", 1, 0);
            List<string> q2 = ExportFieldStr("payment_type", "name", 1, 0);
            int cnt = q1.Count;
            Tpayment_type[] t1 = new Tpayment_type[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].code = q1[i];
                t1[i].name = q2[i];
            }
            data.payment_type = t1;
        }
        private void Table_territories()
        {
            List<int> q1 = ExportFieldInt("territory", "map_oid", 0, 2);
            List<string> q2 = ExportFieldStr("territory", "name", 1, 0);
            int cnt = q1.Count;
            Tterritory[] t1 = new Tterritory[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].map_oid = q1[i];
                t1[i].name = q2[i];
            }
            data.territory = t1;

            List<string> q3 = ExportFieldStr("territory_alias", "alias", 1, 0);
            List<int> q4 = ExportFieldInt("territory_alias", "territory", 0, 2);
            int cnt2 = q3.Count;
            Tterritory_alias[] t2 = new Tterritory_alias[cnt2];
            for (int i = 0; i < cnt2; i++)
            {
                t2[i].alias = q3[i];
                t2[i].territory = q4[i];
            }
            data.territory_alias = t2;
        }
        private void Table_crossroad()
        {
            List<string> q1 = ExportFieldStr("crossroad", "name", 1, 0);
            List<int> q2 = ExportFieldInt("crossroad", "map_oid", 0, 2);
            List<int> q3 = ExportFieldInt("crossroad", "importance", 0, 1);
            int cnt = q1.Count;
            Tcrossroad[] t1 = new Tcrossroad[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].name = q1[i];
                t1[i].map_oid = q2[i];
                t1[i].importance = q3[i];
            }
            data.crossroad = t1;
        }
        private void Table_district()
        {
            List<string> q1 = ExportFieldStr("district", "name", 1, 0);
            List<int> q2 = ExportFieldInt("district", "map_oid", 0, 2);
            List<int> q3 = ExportFieldInt("district", "city", 0, 1);
            int cnt = q1.Count;
            Tdistrict[] t1 = new Tdistrict[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].name = q1[i];
                t1[i].map_oid = q2[i];
                t1[i].city = q3[i];
            }
            data.district = t1;
        }
        private void Table_larials()
        {
            List<string> q1 = ExportFieldStr("larial", "name", 1, 0);
            List<int> q2 = ExportFieldInt("larial", "map_oid", 0, 2);
            int cnt = q1.Count;
            Tlarial[] t1 = new Tlarial[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].name = q1[i];
                t1[i].map_oid = q2[i];
            }
            data.larial = t1;

            List<string> q5 = ExportFieldStr("larial_alias", "alias", 1, 0);
            List<int> q6 = ExportFieldInt("larial_alias", "larial", 0, 2);
            int cnt2 = q5.Count;
            Tlarial_alias[] t2 = new Tlarial_alias[cnt2];
            for (int i = 0; i < cnt2; i++)
            {
                t2[i].alias = q5[i];
                t2[i].larial = q6[i];
            }
            data.larial_alias = t2;
        }
        private void List_blds()
        {
            List<string> q1 = ExportFieldStr("bld_name", "name", 1, 0);
            int cnt = q1.Count;
            string[] t1 = new string[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i] = q1[i];
            }
            data.lists.bld_name = t1;

            List<string> q2 = ExportFieldStr("bld_purpose", "name", 1, 0);
            int cnt2 = q2.Count;
            string[] t2 = new string[cnt2];
            for (int i = 0; i < cnt2; i++)
            {
                t2[i] = q2[i];
            }
            data.lists.bld_purpose = t2;
        }
        private void List_phone_format()
        {
            List<string> q1 = ExportFieldStr("phone_format", "format", 1, 0);
            int cnt = q1.Count;
            string[] t1 = new string[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i] = q1[i];
            }
            data.lists.phone_format = t1;
        }
        private void Table_station_type()
        {
            List<string> q1 = ExportFieldStr("station_type", "entrance_header_plural", 1, 0);
            List<string> q2 = ExportFieldStr("station_type", "entrance_header_singl", 1, 0);
            List<string> q3 = ExportFieldStr("station_type", "name", 1, 0);
            List<string> q4 = ExportFieldStr("station_type", "tab_title", 1, 0);
            List<int> q5 = ExportFieldInt("station_type", "type", 0, 2);
            int cnt = q1.Count;
            Tstation_type[] t1 = new Tstation_type[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].entrance_header_plural = q1[i];
                t1[i].entrance_header_singl = q2[i];
                t1[i].name = q3[i];
                t1[i].tab_title = q4[i];
                t1[i].type = q5[i];
            }
            data.station_type = t1;

            List<string> q15 = ExportFieldStr("tr_type_info", "name", 1, 0);
            List<int> q16 = ExportFieldInt("tr_type_info", "type", 0, 2);
            int cnt2 = q5.Count;
            Ttr_type_info[] t2 = new Ttr_type_info[cnt2];
            for (int i = 0; i < cnt2; i++)
            {
                t2[i].name = q15[i];
                t2[i].type = q16[i];
            }
            data.tr_type_info = t2;
        }
        private void Table_poi()
        {
            List<string> q1 = ExportFieldStr("poi", "tag", 1, 0);
            List<int> q2 = ExportFieldInt("poi", "building_map_oid", 0, 2);
            List<int> q3 = ExportFieldInt("poi", "map_oid", 0, 2);
            int cnt = q1.Count;
            Tpoi[] t1 = new Tpoi[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].tag = q1[i];
                t1[i].building_map_oid = q2[i];
                t1[i].map_oid = q3[i];
            }
            data.poi = t1;
        }
        private void Table_map_layer()
        {
            List<string> q1 = ExportFieldStr("map_layer", "code", 1, 0);
            List<int> q2 = ExportFieldInt("map_layer", "add", 0, 2);
            List<int> q3 = ExportFieldInt("map_layer", "mul", 0, 2);
            int cnt = q1.Count;
            Tmap_layer[] t1 = new Tmap_layer[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].code = q1[i];
                t1[i].add = q2[i];
                t1[i].mul = q3[i];
            }
            data.map_layer = t1;
        }
        private void Table_extra_attr_alias()
        {
            List<string> q1 = ExportFieldStr("extra_attr_alias", "alias", 1, 0);
            List<int> q2 = ExportFieldInt("extra_attr_alias", "extra_attr", 0, 2);
            int cnt = q1.Count;
            Textra_attr_alias[] t1 = new Textra_attr_alias[cnt];
            for (int i = 0; i < cnt; i++)
            {
                t1[i].alias = q1[i];
                t1[i].extra_attr = q2[i];
            }
            data.extra_attr_alias = t1;
        }

        /// <summary>
        /// чтение данных из собранных строк данных в структуру
        /// </summary>
        private void ReadData()
        {
            Table_address_elem();
            Table_org();
            Table_fil();
            Table_fil_contact();
            List_wrk_time();
            Table_map_drilldown();
            List_address_elem_to_map_drilldown();
            List_ad_warnings();
            Table_bld_info();
            Table_building_address();
            Table_service_org();
            Table_building();
            Table_landmark();
            Table_plusones();
            Table_rubrics();
            Table_streets();
            Table_cities();
            Table_sights();
            Table_fil_address();
            Table_fil_payment();
            Table_payment_type();
            Table_territories();
            Table_crossroad();
            Table_district();
            Table_larials();
            List_blds();
            List_phone_format();
            Table_station_type();
            Table_poi();
            Table_map_layer();
            Table_extra_attr_alias();
        }

        /// <summary>
        /// выполняет тест типа значений в поле таблицы
        /// </summary>
        /// <param name="s1">таблица</param>
        /// <param name="s2">поле</param>
        /// <returns>строка с возможными типами значений</returns>
        private string TestParam(string s1, string s2, bool do_not_dexor = false)
        {
            string res = s1+" / "+s2+" - ";
            List<string> ss = new List<string>();
            List<int> ii = new List<int>();
            try { ss = ExportFieldStr(s1, s2, 0, 0, do_not_dexor); res = res + " s00: " + ss.Count; } catch { res = res + " "; }
            try { ss = ExportFieldStr(s1, s2, 0, 3, do_not_dexor); res = res + " s03: " + ss.Count; } catch { res = res + " "; }
            try { ss = ExportFieldStr(s1, s2, 1, 0, do_not_dexor); res = res + " s10: " + ss.Count; } catch { res = res + " "; }
            try { ss = ExportFieldStr(s1, s2, 1, 3, do_not_dexor); res = res + " s13: " + ss.Count; } catch { res = res + " "; }
            try { ii = ExportFieldInt(s1, s2, 0, 1, do_not_dexor); res = res + " i01: " + ii.Count; } catch { res = res +  " "; }
            try { ii = ExportFieldInt(s1, s2, 0, 2, do_not_dexor); res = res + " i02: " + ii.Count; } catch { res = res +  " "; }
            try { ii = ExportFieldInt(s1, s2, 0, 4, do_not_dexor); res = res + " i04: " + ii.Count; } catch { res = res +  " "; }
            try { ii = ExportFieldInt(s1, s2, 0, 10, do_not_dexor); res = res + " i10: " + ii.Count; } catch { res = res + " "; }
            return res;
        }

        /// <summary>
        /// возвращает количество элементов в данных из элемента dump
        /// </summary>;
        /// <param name="id">ид</param>
        /// <returns>количество</returns>
        private int GetCountRows(string id)
        {
            foreach(raw r in dump)
            {
                if (r.id == id) { return r.data.Count; }
            }
            return -1;
        }

        /// <summary>
        /// конструктор. читает реестр, возвращает список городов для выбора
        /// </summary>
        /// <returns>список городов</returns>
        public dgdat()
        {
            filepaths = GetFiles();
            cities = GetCityNames(filepaths);
            if (filepaths.Count()==0) { error += "Не нашли установленных файлов 2ГИС\r\n"; }
        }

        /// <summary>
        /// получает выборку данных по имени таблицы/поля
        /// </summary>
        /// <param name="name">имя таблицы/поля</param>
        /// <returns>список строк с выбранными данными</returns>
        public List<string> GetItemsByName(string name)
        {
            foreach(raw q in dump)
            {
                if (q.id == name)
                {
                    return q.data;
                }
            }
            return new List<string>();
        }

        /// <summary>
        /// по указанному городу открывает и проверяет файл данных
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public bool OpenCheck(string city)
        {
            string file = GetFileByCity(city);
            fp = new FileStream(file, FileMode.Open, FileAccess.Read);

            uint id1 = ReadByte();
            uint id2 = ReadByte();
            uint id3 = ReadByte();
            uint id4 = ReadByte();
            uint id5 = ReadByte();
            if (!((id1 == 0x07) && (id2 == 0x47) && (id3 == 0x44) && (id4 == 0x46) && (id5 == 0xef)))
            {
                error += "Сигнатура файла данных не верная\r\n";
                Ready = false;
            }
            else
            {
                Ready = true;
            }
            return Ready;
        }

        /// <summary>
        /// основной код чтения файла
        /// </summary>
        public Tdata Read()
        {
            if (!Ready) { return data; }
            // основной код разбора файла

            // пропускаем ненужные данные
            uint l1 = ReadLong();
            uint l2 = ReadLong();
            uint pv1 = ReadPackedValue();
            uint pv2 = ReadPackedValue();
            uint pv3 = ReadPackedValue();
            uint pv4 = ReadPackedValue();

            // читаем строку с именами таблиц
            uint tbllen = ReadByte();
            string tbl = ReadString(tbllen);
            // разбираем имена таблиц
            while (tbl.Length != 0)
            {
                int len = (byte)tbl[0];
                tbl = tbl.Substring(1);
                string chunk = tbl.Substring(0, len);
                tbl = tbl.Substring(len);
                int size = (int)GetPackedValue(ref tbl);
                string temp = ReadString((uint)size);
            }

            // ищем точки data и opt
            uint temp1 = ReadPackedValue();
            tbllen = ReadByte();
            tbl = ReadString(tbllen);
            // разбираем имена таблиц
            while (tbl.Length != 0)
            {
                int len = (byte)tbl[0];
                tbl = tbl.Substring(1);
                string chunk = tbl.Substring(0, len);
                tbl = tbl.Substring(len);
                int size = (int)GetPackedValue(ref tbl);
                if (chunk == "data")
                {
                    root = fp.Position;
                }
                string temp2 = ReadString((uint)size);
            }

            // Processing root table (data)
            fp.Seek(root, SeekOrigin.Begin);
            tbllen = ReadPackedValue();
            tbl = ReadString(tbllen);
            while (tbl.Length != 0)
            {
                int len = (byte)tbl[0];
                tbl = tbl.Substring(1);
                string chunk = tbl.Substring(0, len);
                tbl = tbl.Substring(len);
                int size = (int)GetPackedValue(ref tbl);
                string data = ReadString((uint)size);
                ProcessTable(chunk, data);
            }

            ReadData();
            return data;
        }

        /// <summary>
        /// добавляет данные в структуру
        /// </summary>
        /// <param name="i">имя таблицы_поля</param>
        /// <param name="d">список значений</param>
        private void AddDump(string id, List<string> d)
        {
            raw t1 = new raw();
            t1.id = id;
            t1.data = d;
            dump.Add(t1);

            int fl = -1;
            for (int i = 0; i < dump.Count; i++)
            {
                if (dump[i].id == id) { fl = i; }
            }
            if (fl == -1)
            {
                dump.Add(t1);
            }
            else
            {
                dump[fl] = t1;
            }
        }

        /// <summary>
        /// добавляет данные в структуру
        /// </summary>
        /// <param name="t">имя таблицы</param>
        /// <param name="f">имя поля</param>
        /// <param name="d">данные</param>
        private void AddDataDir(string t, string f, string d)
        {
            dd t1 = new dd();
            t1.table = t;
            t1.field = f;
            t1.data = d;

            int fl = -1;
            for(int i=0; i<datadir.Count; i++)
            {
                if ((datadir[i].table == t) && (datadir[i].field == f)) { fl = i; }
            }
            if (fl == -1)
            {
                datadir.Add(t1);
            } else
            {
                datadir[fl] = t1;
            }
        }

        /// <summary>
        /// перевод utf16 в utf8
        /// </summary>
        /// <param name="utf16String">строка utf16</param>
        /// <returns>строка utf8</returns>
        private string Utf16ToUtf8(string utf16String)
        {
            byte[] utf16Bytes = Encoding.ASCII.GetBytes(utf16String);
            byte[] utf8Bytes = Encoding.Convert(Encoding.ASCII, Encoding.UTF8, utf16Bytes);
            return Encoding.Unicode.GetString(utf8Bytes);
        }

        /// <summary>
        /// обработка основных таблиц
        /// </summary>
        /// <param name="name">имя таблицы</param>
        /// <param name="data">данные</param>
        private void ProcessTable(string name, string data)
        {
            foreach (string item in do_not_export)
            {
                if (name.IndexOf(item) == 0)
                {
                    //Console.WriteLine(name);//122 geo_
                    return;
                }
            }

            string zcv = data;
            int tbllen = (int)GetPackedValue(ref data);
            string tbl = data.Substring(0, tbllen);
            data = data.Substring(tbllen);

            while (tbl.Length != 0)
            {
                int len = (byte)tbl[0];
                tbl = tbl.Substring(1);
                string chunk = tbl.Substring(0, len);
                tbl = tbl.Substring(len);
                if (chunk == "data")
                {
                    AddDataDir(name, chunk, zcv);//было zcv
                    return;
                }
                int size = (int)GetPackedValue(ref tbl);
                string p = data.Substring(0, size);
                data = data.Substring(size);
                string data3 = DexorTable(name, chunk, p, 0);
                //AddDataDir(name, "data3", data3);
            }
        }

        /// <summary>
        /// расшифровка таблиц
        /// </summary>
        /// <param name="mainname">имя таблицы</param>
        /// <param name="fieldname">имя поля</param>
        /// <param name="data">строка с данными</param>
        /// <param name="need_decode">флаг необходимости xor</param>
        /// <returns>строка с расшифрованными данными</returns>
        private string DexorTable(string mainname, string fieldname, string data, int need_decode=1)
        {
            string copy = data;
            if (fieldname != "")
            {
                AddDataDir(mainname, fieldname, data);
            }
            uint tbllen = GetPackedValue(ref data);
            if (tbllen == 0)
            {
                return "";
            }

            uint len = GetPackedValue(ref data);
            data = data.Substring((int)len);
            len = GetPackedValue(ref data);
            int start = (int)tbllen + 1;

            StringBuilder sb = new StringBuilder();
            for (int i=start; i<(start+len); i++)
            {
                byte ch = (byte)copy[i];
                if(need_decode == 1)
                {
                    sb.Append((char)(ch ^ 0xC5));
                }
                else
                {
                    sb.Append((char)(ch));
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// получает строку данных из datadir
        /// </summary>
        /// <param name="name">имя таблицы</param>
        /// <param name="field">имя поля</param>
        /// <returns>строка данных</returns>
        private string GetDataDirValue(string name, string field)
        {
            foreach (dd item in datadir)
            {
                if ((item.table == name) && (item.field == field))
                {
                    return item.data;
                }
            }
            return "";
        }

        /// <summary>
        /// создает список чисел элементов поля таблицы
        /// </summary>
        /// <param name="name">имя таблицы</param>
        /// <param name="field">имя поля</param>
        /// <param name="need_decode">признак необходимости xor данных</param>
        /// <param name="pair_decode">тип декодирования</param>
        /// <returns>списов чисел со значениями полей</returns>
        private List<int> ExportFieldInt(string name, string field, int need_decode = 1, int pair_decode = 0, bool do_not_dexor = false)
        {
            List<int> afield = new List<int>();

            string dat = GetDataDirValue(name, field);
            if (dat.Length == 0)
            {
                return afield;
            }

            if (!do_not_dexor)
            {
                dat = DexorTable(name, field, dat, need_decode);
            }

            if (pair_decode == 10)
            {
                for (int k = 0; k < dat.Length; k += 4)
                {
                    byte b1 = (byte)dat[k];
                    byte b2 = (byte)dat[k + 1];
                    byte b3 = (byte)dat[k + 2];
                    byte b4 = (byte)dat[k + 3];
                    afield.Add(b1 + 256 * b2 + 256 * 256 * b3 + 256 * 256 * 256 * b4);
                }
                return afield;
            }
            if (pair_decode == 11)
            {
                for (int k = 0; k < dat.Length; k += 4)
                {
                    byte b4 = (byte)dat[k];
                    byte b3 = (byte)dat[k + 1];
                    byte b2 = (byte)dat[k + 2];
                    byte b1 = (byte)dat[k + 3];
                    afield.Add(b1 + 256 * b2 + 256 * 256 * b3 + 256 * 256 * 256 * b4);
                }
                return afield;
            }

            if (pair_decode == 1)
            {
                for (int k = 0; k < dat.Length; k++)
                {
                    string temp = "";
                    if (dat.Length - k > 12)
                    {
                        temp = dat.Substring(k, 12);
                    }
                    else
                    {
                        temp = (dat + "\0\0\0\0\0\0\0\0\0\0\0\0").Substring(k, 12);
                    }
                    int initlen = temp.Length;
                    uint repeat = GetPackedValue(ref temp);
                    int value = (int)GetPackedValue(ref temp);
                    for (int n = 0; n < repeat; n++)
                    {
                        afield.Add(value);
                    }
                    k = k + (initlen - temp.Length) - 1;
                }
                return afield;
            }

            if (pair_decode == 4)
            {
                for (int k = 0; k < dat.Length; k++)
                {
                    string temp = "";
                    if (dat.Length - k > 22)
                    {
                        temp = dat.Substring(k, 22);
                    }
                    else
                    {
                        temp = (dat + "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0").Substring(k, 22);
                    }
                    int initlen = temp.Length;
                    int repeat = (int)GetPackedValue(ref temp);
                    int nm = initlen - temp.Length;
                    int len = (int)GetPackedValue(ref temp);
                    int xlen = (int)GetPackedValue(ref temp);
                    for (int n = 0; n < repeat; n++)
                    {
                        afield.Add(xlen);
                    }
                    k = k + len + nm;
                }
                return afield;
            }

            if (pair_decode == 2)
            {
                for (int k = 0; k < dat.Length; k++)
                {
                    string temp = "";
                    if (dat.Length - k > 12)
                    {
                        temp = dat.Substring(k, 12);
                    }
                    else
                    {
                        temp = (dat + "\0\0\0\0\0\0\0\0\0\0\0\0").Substring(k, 12);
                    }
                    int initlen = temp.Length;
                    int value = (int)GetPackedValue(ref temp);
                    afield.Add(value);
                    k = k + (initlen - temp.Length) - 1;
                }
                return afield;
            }

            return afield;
        }

        /// <summary>
        /// создает список строк элементов поля таблицы
        /// </summary>
        /// <param name="name">имя таблицы</param>
        /// <param name="field">имя поля</param>
        /// <param name="need_decode">признак необходимости xor данных</param>
        /// <param name="pair_decode">тип декодирования</param>
        /// <returns>списов строк со значениями полей</returns>
        private List<string> ExportFieldStr(string name, string field, int need_decode = 1, int pair_decode = 0, bool do_not_dexor = false)
        {
            List<string> afield = new List<string>();

            string dat = GetDataDirValue(name, field);
            if (dat.Length == 0)
            {
                return afield;
            }
            if (!do_not_dexor)
            {
                dat = DexorTable(name, field, dat, need_decode);
            }
            if (pair_decode == 3)
            {
                for (int k = 0; k < dat.Length; k++)
                {
                    string temp = "";
                    if (dat.Length - k > 12)
                    {
                        temp = dat.Substring(k, 12);
                    }
                    else
                    {
                        temp = (dat + "\0\0\0\0\0\0\0\0\0\0\0\0").Substring(k, 12);
                    }
                    int initlen = temp.Length;
                    int repeat = (int)GetPackedValue(ref temp);
                    int nm = initlen - temp.Length;
                    int len = (int)GetPackedValue(ref temp);
                    string x = "";
                    if (len == 0)
                    {
                        x = "";
                    }
                    else
                    {
                        if (len > 0x7f)
                        {
                            x = dat.Substring(k + nm, len + 2);
                            x = UnpackWideString(x);
                            len++;
                        }
                        else
                        {
                            x = dat.Substring(k + nm, len + 1);
                            x = UnpackWideString(x);
                        }
                    }
                    for (int n = 0; n < repeat; n++)
                    {
                        afield.Add(Utf16ToUtf8(x));
                    }
                    k = k + len + nm;
                }
                return afield;
            }

            for (int k = 0; k < dat.Length; k++)
            {
                string temp = "";
                if (dat.Length - k > 5)
                {
                    temp = dat.Substring(k, 5);
                }
                else
                {
                    temp = (dat + "\0\0\0\0\0\0\0\0\0\0\0\0").Substring(k, 5);
                }
                int len = (int)GetPackedValue(ref temp);
                string x = "";
                if (len == 0)
                {
                    x = "";
                }
                else
                {
                    if (len > 0x7f)
                    {
                        x = dat.Substring(k, len + 2);
                        x = UnpackWideString(x);
                        len++;
                    }
                    else
                    {
                        x = dat.Substring(k, len + 1);
                        x = UnpackWideString(x);
                    }
                }
                afield.Add(Utf16ToUtf8(x));
                k = k + len;
            }
            return afield;
        }

        /// <summary>
        /// получает путь и имя файла по названию города
        /// </summary>
        /// <param name="city">город</param>
        /// <returns>путь+имя</returns>
        private string GetFileByCity(string city)
        {
            for(int i=0; i<cities.Count; i++)
            {
                if (cities[i] == city)
                {
                    return filepaths[i];
                }
            }
            return "";
        }

        /// <summary>
        /// заменяет символ в строке
        /// </summary>
        /// <param name="s">строка</param>
        /// <param name="i">позиция символа</param>
        /// <param name="c">новый символ</param>
        /// <returns>строка с замененным символом</returns>
        private string ReplaceChar(string s, int i, char c)
        {
            if (s.Length < i) { return s; }
            return s.Substring(0, i) + c + s.Substring(i+1);
        }

        /// <summary>
        /// распаковывает строку
        /// </summary>
        /// <param name="tbl">строка</param>
        /// <returns>строка</returns>
        private string UnpackWideString(string str)
        {
            uint x1 = GetPackedValue(ref str);
            uint x2 = GetPackedValue(ref str);

            StringBuilder zz = new StringBuilder();
            string z = "";

            for (int i = 0; i < x2; i++)
            {
                zz.Append(str[i]);
                zz.Append('\0');
            }
            str = str.Substring((int)x2);

            z = zz.ToString();
            int l = z.Length;

            if (str.Length != 0)
            {
                List<byte> arr = new List<byte>();
                int count = (byte)str[0];
                str = str.Substring(1);
                int mcount = count;
                for (int i=0; i<mcount; i++)
                {
                    arr.Add((byte)str[0]);
                    str = str.Substring(1);
                }
                int iter = 0;
                int offset = 0;
                while (str.Length != 0)
                {
                    int v = (byte)str[0];
                    str = str.Substring(1);
                    count = (int)Math.Floor((decimal)(v / mcount));
                    offset = v % mcount;
                    char cfr = (char)(arr[offset]);
                    if (count == 0)
                    {
                        for (int i=iter; i<l; i+=2)
                        {
                            z = ReplaceChar(z, i+1, cfr);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < count; i++)
                        {
                            z = ReplaceChar(z, iter+1, cfr);
                            iter += 2;
                        }
                    }
                }
            }
            return z;
        }

        /// <summary>
        /// получает упакованное целое из строки
        /// </summary>
        /// <param name="fp">поток</param>
        /// <param name="tbl">ссылка на строку</param>
        /// <returns>число</returns>
        private uint GetPackedValue(ref string tbl)
        {
            uint size = (byte)tbl[0];
            tbl = tbl.Substring(1);
            uint val = 0;

            if (size >= 0xf0)
            {
                uint b1 = (byte)tbl[0];
                uint b2 = (byte)tbl[1];
                uint b3 = (byte)tbl[2];
                uint b4 = (byte)tbl[3];
                tbl = tbl.Substring(4);
                val = b1 * 256 * 256 * 256 + b2 * 256 * 256 + b3 * 256 + b4;
                return val;
            }
            if (size >= 0xe0)
            {
                uint b2 = (byte)tbl[0];
                uint b3 = (byte)tbl[1];
                uint b4 = (byte)tbl[2];
                tbl = tbl.Substring(3);
                val = (size ^ 0xe0) * 256 * 256 * 256 + b2 * 256 * 256 + b3 * 256 + b4;
                return val;
            }
            if (size >= 0xc0)
            {
                uint b3 = (byte)tbl[0];
                uint b4 = (byte)tbl[1];
                tbl = tbl.Substring(2);
                val = (size ^ 0xc0) * 256 * 256 + b3 * 256 + b4;
                return val;
            }
            if (size >= 0x80)
            {
                uint b4 = (byte)tbl[0];
                tbl = tbl.Substring(1);
                val = (size ^ 0x80) * 256 + b4;
                return val;
            }
            val = size;
            return val;
        }

        /// <summary>
        /// читает упакованное целое
        /// </summary>
        /// <param name="fp">поток</param>
        /// <returns>число</returns>
        private uint ReadPackedValue()
        {
            uint size = ReadByte();
            uint val = size;
            if( size >= 0xf0)
            {
                uint b1 = ReadByte();
                uint b2 = ReadByte();
                uint b3 = ReadByte();
                uint b4 = ReadByte();
                val = b1 * 256 * 256 * 256 + b2 * 256 * 256 + b3 * 256 + b4;
                return val;
            }
            if (size >= 0xe0)
            {
                uint b2 = ReadByte();
                uint b3 = ReadByte();
                uint b4 = ReadByte();
                val = (size ^ 0xe0) * 256 * 256 * 256 + b2 * 256 * 256 + b3 * 256 + b4;
                return val;
            }
            if (size >= 0xc0)
            {
                uint b3 = ReadByte();
                uint b4 = ReadByte();
                val = (size ^ 0xc0) * 256 * 256 + b3 * 256 + b4;
                return val;
            }
            if (size >= 0x80)
            {
                uint b4 = ReadByte();
                val = (size ^ 0x80) * 256 + b4;
                return val;
            }
            return val;
        }

        /// <summary>
        ///  читает число в один байт
        /// </summary>
        /// <param name="fp">поток</param>
        /// <returns>число, байт</returns>
        private uint ReadByte()
        {
            return (uint)fp.ReadByte();
        }

        /// <summary>
        ///  читает длинное целое
        /// </summary>
        /// <param name="fp">поток</param>
        /// <returns>число</returns>
        private uint ReadLong()
        {
            uint i1 = ReadByte();
            uint i2 = ReadByte();
            uint i3 = ReadByte();
            uint i4 = ReadByte();
            return i1 + 256 * i2 + 256 * 256 * i3 + 256 * 256 * 256 * i4;
        }

        /// <summary>
        ///  читает строку указанной длинны
        /// </summary>
        /// <param name="fp">поток</param>
        /// <param name="len">длина</param>
        /// <returns>строка</returns>
        private string ReadString(uint len)
        {
            if (len > 0)
            {
                StringBuilder sb = new StringBuilder();
                char[] arrc = new char[len];
                byte[] arrb = new byte[len];
                fp.Read(arrb, 0, (int)len);
                for(int i = 0; i < len; i++) { arrc[i] = (char)arrb[i]; }
                sb.Append(arrc);
                return sb.ToString();
            }
            return "";
        }

        /// <summary>
        /// выделяет названия городов из наименований файлов
        /// </summary>
        /// <param name="files">массив имен файлов</param>
        /// <returns>массив названий городов</returns>
        private List<string> GetCityNames(List<string> files)
        {
            List<string> names = new List<string>();
            foreach(string t1 in files)
            {
                string t2 = t1.Substring(0, t1.Length - 6);
                names.Add(t2.Substring(t2.LastIndexOf("\\Data_")+6));
            }
            return names;
        }

        /// <summary>
        /// возвращает имена найденных файлов .dgdat
        /// </summary>
        /// <returns>массив имен найденных файлов .dgdat</returns>
        private List<string> GetFiles()
        {
            string class_name = GetRegistryClassName();
            if (class_name == "")
            {
                return new List<string>();
            }
            string path_files = GetRegistryDgdatPath(class_name);
            if (path_files == "")
            {
                return new List<string>();
            }
            return GetDgdatFiles(path_files);
        }

        /// <summary>
        /// полные пути к найденным файлам .dgdat
        /// </summary>
        /// <returns>массив путей к файлам .dgdat</returns>
        private List<string> GetDgdatFiles(string path)
        {
            return System.IO.Directory.GetFiles(path, "*.dgdat").ToList();
        }

        /// <summary>
        /// возвращает путь к файлам .dgdat из реестра по имени класса обработчика
        /// </summary>
        /// <returns>путь к файлам .dgdat или пустую строку при ошибке</returns>
        private string GetRegistryDgdatPath(string class_name)
        {
            Microsoft.Win32.RegistryKey hklm = Microsoft.Win32.Registry.LocalMachine;
            Microsoft.Win32.RegistryKey path = hklm.OpenSubKey("SOFTWARE\\Classes\\"+ class_name + "\\shell\\Open\\command", false);
            hklm.Close();
            if (path != null)
            {
                string path_str = path.GetValue("").ToString();
                return path_str.Substring(0, path_str.LastIndexOf('\\'));
            }
            error += "Не нашли класс реестра SOFTWARE\\Classes\\"+ class_name + "\r\n";
            return "";
        }

        /// <summary>
        /// возвращает имя класса реестра обработчика для расширения .dgdat
        /// </summary>
        /// <returns>имя класса реестра или пустую строку при ошибке</returns>
        private string GetRegistryClassName()
        {
            Microsoft.Win32.RegistryKey hklm = Microsoft.Win32.Registry.LocalMachine;
            Microsoft.Win32.RegistryKey dgdat = hklm.OpenSubKey("SOFTWARE\\Classes\\.dgdat", false);
            hklm.Close();
            if (dgdat != null)
            {
                return dgdat.GetValue("").ToString();
            }
            error += "Не нашли класс реестра SOFTWARE\\Classes\\.dgdat\r\n";
            return "";
        }

    }
}
