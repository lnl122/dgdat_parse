# Парсер файлов .dgdat 
## Цель
Выполнять парсинг локальных файлов данных 2gis (.dgdat) для выборки данных.

## Идея
Взята идея [mbry](https://github.com/mbry) из проекта [DgdatToXlsx](https://github.com/mbry/DgdatToXlsx)

Код переписан на c# в отдельном модуле, готовом для использоания в других проектах.

## Использование
### получение списка файлов/городов, установленных на ПК

`dgdat d = new dgdat();`

`List<string> cities = d.cities;`

Пример значений списка:
- Almaty
- Bishkek
- Krasnoyarsk
- Rostov

Пользователь должен произвести выбор необходимого города для дальнейшей его обработки.

Пустой список возвращается в случаях:
- 2гис не установлен
- версия 2гис не 3.0
- реестр заполнен некорректно
- нет установленных баз городов

### Обработка файла одного города

Выбранный город передаётся в функцию OpenCheck, проверяющую доступность файла и сигнатуру формата

`bool fl = d.OpenCheck("Almaty");`

Возвращаемое значение `true` показывает на допустимость дальнейшей обработки

Обработка файла - вызов процедуры `Read`

`dgdat.Tdata data = d.Read();`

*(!) Обработка может занимать до 10-15 секунд для больших городов, до 5-7 секунд для городов в 0,5-1 млн жителей.*

Доступ к обработанным данным возможно получить через полученную структуру `data`.

Пример получения списка организаций, имеющих в названии текст "777", с адресами:

`  string name_mask = "777";

  for (int org_idx = 0; org_idx < data.org.Length; org_idx++) {

    if (data.org[org_idx].name.IndexOf(name_mask) != -1) {

      for(int fil_idx=0; fil_idx < data.fil.Length; fil_idx++) {

        if(data.fil[fil_idx].org == org_idx + 1) {

          for (int addr_idx=0; addr_idx < data.fil_address.Length; addr_idx++) {

            if(data.fil_address[addr_idx].fil == fil_idx + 1) {

              Console.WriteLine("OrgName={0}, Street={1}, House={2}, Fil={3}{4}",

                data.org[org_idx].name, 

                data.street[data.address_elem[data.fil_address[addr_idx].address - 1].street-1].name,

                data.address_elem[data.fil_address[addr_idx].address - 1].building,

                data.fil[fil_idx].title,

                data.fil[fil_idx].office);

            }

          }

        }

      }

    }

  }`

результат работы кода для БД г.Алматы (Казахстан):

- OrgName=Асель 777, производственная фирма, Street=Ухтомского, House=21, Fil=
- OrgName=Корунд-777, брокерская компания, Street=Думан 2-й микрорайон, House=9, Fil=54 офис
- OrgName=Росс 777 Сервис, СТО, Street=Розыбакиева, House=105в, Fil=
- OrgName=RAZBOR 777, салон авторазбора Toyota, Nissan, Mitsubishi, ИП Глянц С.В., Street=Рыскулова проспект, House=57в, Fil=1 этаж
- OrgName=777 Ломбард, ТОО, автоломбард, Street=Масанчи, House=85, Fil=8 офис
- OrgName=777, автомагазин, Street=Райымбека проспект, House=82, Fil=
- OrgName=777, кафе, Street=Суюнбая проспект, House=152Б, Fil=1 этаж
- OrgName=777, кафе, ИП Аляев М.Е., Street=Гагарина проспект, House=206ж, Fil=
- OrgName=777, кафе-бар, Street=Спортивная, House=1а, Fil=
- OrgName=777, магазин продуктов, Street=Жангельдина, House=190а, Fil=
- OrgName=777, магазин разливного пива, Street=Токтабаева, House=7, Fil=
- OrgName=777, магазин хозяйственных товаров, Street=Кокжиек микрорайон, House=9, Fil=
- OrgName=777, продовольственный магазин, ИП Абдрахманов А.Б., Street=Акан Серы, House=147, Fil=

*(!) Для накоторых элементов тектовых наименований будет необходима дополнительная обработка в виде замены символов "?" на пробел.*

### Связи между таблицами

Связи между таблицами, содержащих данные возможно найти в базе знаний на [странице описания API](http://plugins.2gis.ru/wiki/Table) 2GIS

